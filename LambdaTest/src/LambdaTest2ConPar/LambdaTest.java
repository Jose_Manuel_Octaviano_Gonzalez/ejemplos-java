/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package LambdaTest2ConPar;

/**
 *
 * @author joseo
 */
public class LambdaTest {
    public static  void main(String[] args){
        
        ///codigo de primer ejemplo lambda con parametros
        Operaciones op = (num1, num2) -> System.out.println("Suma: " +(num1 + num2));
        
        //op.imprimirSuma(5, 10); //se comento para realizar ejemplo 2
        
        /////Ejemplo 2 lambda con parametros
        /*LambdaTest obj = new LambdaTest();
        obj.miMetodo(op, 10, 10); */  //Se comenta para realizar el ejemplo 3
        
        /////////EJEMPLO 3 CON MEJORAS
        op.imprimirSuma(5, 10);
        LambdaTest obj = new LambdaTest();
        obj.miMetodo((num1, num2) -> System.out.println("Resta: " + (num1 - num2)), 20, 10);
        obj.miMetodo((num1, num2) -> System.out.println("Multiplicación: " + (num1 * num2)), 20, 10);
        obj.miMetodo((num1, num2) -> System.out.println("División: " + (num1 / num2)), 20, 10);
        
        
    }
    
    /// CREACIÓN DE MÉTODO PARA LA SUMA DE LOS NÚMEROS PARA EJEMPLO 2
    public void miMetodo(Operaciones op, int num1, int num2){
        op.imprimirSuma(num1, num2);
    }
}
