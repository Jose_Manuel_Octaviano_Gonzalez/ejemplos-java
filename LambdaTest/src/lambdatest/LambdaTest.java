/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lambdatest;

/**
 *
 * @author joseo
 */
public class LambdaTest {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        FuctionTest1 ft =() -> System.out.println("Hola mundo");
        
        // PRIMER EJEMPLO 
        //ft.saludar();
        
        //Para realizar segundo ejemplo comente la variable que utiloz para el primer ejemplo
        //SEGUNDO EJEMPLO DE LAMBDA SIN PARAMETROS
        
        LambdaTest obj = new LambdaTest();
        obj.miMetodo(ft);
          
    }
    
    //  METODO CREADO EN EL EJEMPLO 2 PARA EJECUTAR EL CODIGO DEL ARCHIVO FuctionTest1 
    public void miMetodo(FuctionTest1 parametro){
        parametro.saludar();
        
    }
}
